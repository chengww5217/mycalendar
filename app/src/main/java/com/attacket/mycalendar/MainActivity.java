package com.attacket.mycalendar;

import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.codbking.calendar.CaledarAdapter;
import com.codbking.calendar.CalendarBean;
import com.codbking.calendar.CalendarDateView;
import com.codbking.calendar.CalendarUtil;
import com.codbking.calendar.CalendarView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private RecyclerView mRecyclerView;
    private CalendarDateView mCalendarDateView;
    private TextView mTitle;

    private void initView() {
        mTitle = (TextView) findViewById(R.id.tv_title);

        mCalendarDateView = (CalendarDateView) findViewById(R.id.calendarDateView);
        mCalendarDateView.setAdapter(new CaledarAdapter() {
            @Override
            public View getView(View convertView, ViewGroup parentView, CalendarBean bean) {
                TextView view;
                if (convertView == null) {
                    convertView = LayoutInflater.from(parentView.getContext()).inflate(R.layout.item_calendar, null);
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(px(48), px(48));
                    convertView.setLayoutParams(params);
                }

                view = (TextView) convertView.findViewById(R.id.text);

                view.setText("" + bean.day);
                if (bean.mothFlag != 0) {
                    view.setTextColor(0xff999999);
                } else {
                    view.setTextColor(0xff666666);
                }

                return convertView;
            }
        });

        mCalendarDateView.setOnItemClickListener(new CalendarView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int postion, CalendarBean bean) {
                mTitle.setText(bean.year + "年  " + getDisPlayNumber(bean.moth) + "/" + getDisPlayNumber(bean.day));
            }
        });

        int[] data = CalendarUtil.getYMD(new Date());
        mTitle.setText(data[0] + "年  " + data[1] + "月");

        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(this));
        List<TestBean.Test> tests = new ArrayList<>();
        TestBean.Test test = new TestBean.Test("5000", "5000", "");
        TestBean.Test test2 = new TestBean.Test("5000", "5000", "");
        tests.add(test);
        tests.add(test2);
        SimpleAdapter adapter = new SimpleAdapter(tests);
        adapter.bindToRecyclerView(mRecyclerView);
        adapter.addHeaderView(LayoutInflater.from(this)
                .inflate(R.layout.item_header,null));

    }

    class SimpleAdapter extends BaseQuickAdapter<TestBean.Test, BaseViewHolder> {

        public SimpleAdapter(@Nullable List<TestBean.Test> data) {
            super(R.layout.recycler_item_repaying, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, TestBean.Test item) {
            helper.setText(R.id.tv_receivable_principal, item.getT1())
                    .setText(R.id.tv_receivable_interest, item.getT2());
        }
    }

    public static int px(float dipValue) {
        Resources r= Resources.getSystem();
        final float scale =r.getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    private String getDisPlayNumber(int num) {
        return num < 10 ? "0" + num : "" + num;
    }
}
